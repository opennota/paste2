// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"log"
	"net/http"

	"github.com/gorilla/securecookie"
	"golang.org/x/crypto/bcrypt"
)

var (
	hashKey      = securecookie.GenerateRandomKey(64)
	secureCookie = securecookie.New(hashKey, nil)
)

func authMiddleware(username, password string) func(http.HandlerFunc) http.HandlerFunc {
	if password == "" {
		return func(h http.HandlerFunc) http.HandlerFunc { return h }
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		log.Fatal(err)
	}

	return func(next http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			authenticated := false
			if cookie, err := r.Cookie("auth"); err == nil {
				value := make(map[string]string)
				if err := secureCookie.Decode("auth", cookie.Value, &value); err == nil && value["user-agent"] == r.UserAgent() {
					authenticated = true
				}
			}

			if !authenticated {
				user, pass, ok := r.BasicAuth()
				if !ok || bcrypt.CompareHashAndPassword(hashedPassword, []byte(pass)) != nil || user != username {
					w.Header().Add("Www-Authenticate", `Basic realm="Who are you?"`)
					w.WriteHeader(http.StatusUnauthorized)
					return
				}

				if encoded, err := secureCookie.Encode("auth", map[string]string{
					"user-agent": r.UserAgent(),
				}); err == nil {
					cookie := &http.Cookie{
						Name:     "auth",
						Value:    encoded,
						Path:     "/",
						HttpOnly: true,
					}
					if r.Proto == "https" {
						cookie.Secure = true
					}
					http.SetCookie(w, cookie)
				}
			}

			next(w, r)
		}
	}
}
