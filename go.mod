module gitlab.com/opennota/pst

go 1.16

require (
	github.com/chromedp/cdproto v0.0.0-20211107215322-d3760c2dc57b
	github.com/chromedp/chromedp v0.7.4
	github.com/gorilla/securecookie v1.1.1
	go.etcd.io/bbolt v1.3.6
	golang.org/x/crypto v0.0.0-20211108221036-ceb1ce70b4fa
	golang.org/x/sys v0.0.0-20211110154304-99a53858aa08 // indirect
)

// +heroku goVersion go1.17
