// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

//go:build e2e

package main

import (
	"bytes"
	"context"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"time"

	"github.com/chromedp/cdproto/browser"
	"github.com/chromedp/cdproto/cdp"
	"github.com/chromedp/cdproto/log"
	"github.com/chromedp/cdproto/page"
	"github.com/chromedp/cdproto/runtime"
	"github.com/chromedp/chromedp"
	"github.com/chromedp/chromedp/kb"
)

const rootURL = "http://localhost:3000"

const clipboardWriteJS = `
(function(text) {
  navigator.clipboard.writeText(text).then(() => {
    window._clipboardContent = text;
  }).catch(err => {
    console.log('Err: ' + err);
  });
  return true;
})(%q);
`

func copyToClipboard(text string) chromedp.Action {
	var res bool
	return chromedp.Tasks{
		chromedp.Evaluate(fmt.Sprintf(clipboardWriteJS, text), &res),
		chromedp.Query("head", func(s *chromedp.Selector) {
			chromedp.WaitFunc(func(ctx context.Context, cur *cdp.Frame, _ runtime.ExecutionContextID, ids ...cdp.NodeID) ([]*cdp.Node, error) {
				var clipboardContent string
				if err := chromedp.Evaluate("window._clipboardContent || ''", &clipboardContent).Do(ctx); err != nil {
					return nil, err
				} else if clipboardContent != text {
					return nil, nil
				}
				return []*cdp.Node{}, nil
			})(s)
		}),
	}
}

func getNumberOfNotes(n *int) chromedp.Action {
	return chromedp.Evaluate("document.querySelectorAll('.note').length", &n)
}

func selectTextRange(sel string, start, end int) chromedp.Action {
	var res bool
	return chromedp.Evaluate(fmt.Sprintf(`
(function (selector, start, end) {
	const el = document.querySelector(selector);
	const startNode = el.firstChild;
	const endNode = startNode;
	startNode.nodeValue = startNode.nodeValue.trim();

	const range = document.createRange();
	range.setStart(startNode, start);
	range.setEnd(endNode, end + 1);

	const sel = window.getSelection();
	sel.removeAllRanges();
	sel.addRange(range);

	document.dispatchEvent(new Event('selectionchange'));

	return true;
})(%q, %d, %d);`, sel, start, end), &res)
}

func testInternal(t *testing.T, ctx context.Context) {
	ctx, _ = context.WithTimeout(ctx, 60*time.Second)

	// No notes initially, no errors.
	numberOfNotes := 0
	errText := ""
	if err := chromedp.Run(ctx,
		chromedp.Navigate(rootURL),
		getNumberOfNotes(&numberOfNotes),
		chromedp.Text(".error", &errText, chromedp.ByQuery),
	); err != nil {
		t.Fatalf("could not get the number of notes: %v", err)
	}
	if numberOfNotes != 0 {
		t.Fatalf("want no notes, got %d", numberOfNotes)
	}
	if errText != "" {
		t.Fatalf("want no error, got %q", errText)
	}

	// Empty clipboard.
	if err := chromedp.Run(ctx,
		copyToClipboard("  "),
		chromedp.Click(".action-paste", chromedp.ByQuery),
		chromedp.WaitVisible(".error", chromedp.ByQuery),
		chromedp.Text(".error", &errText, chromedp.ByQuery),
		getNumberOfNotes(&numberOfNotes),
	); err != nil {
		t.Fatalf("pasting with the empty clipboard: %v", err)
	}
	if numberOfNotes != 0 {
		t.Fatalf("want no notes, got %d", numberOfNotes)
	}
	if want := "The clipboard is empty."; errText != want {
		t.Fatalf("want the error message to be %q, got %q", want, errText)
	}

	// The first note.
	const textToPaste = "This will be a first note."
	text := ""
	if err := chromedp.Run(ctx,
		copyToClipboard(textToPaste),
		chromedp.Click(".action-paste", chromedp.ByQuery),
		chromedp.WaitVisible(".note[data-id]", chromedp.ByQuery),
		chromedp.Text(".note-content", &text, chromedp.ByQuery),
		getNumberOfNotes(&numberOfNotes),
		chromedp.Text(".error", &errText, chromedp.ByQuery),
	); err != nil {
		t.Fatalf("paste failed: %v", err)
	}
	if numberOfNotes != 1 {
		t.Fatalf("want one note, got %d", numberOfNotes)
	}
	if text != textToPaste {
		t.Fatalf("want pasted note text to be %q, got %q", textToPaste, text)
	}
	if errText != "" {
		t.Fatalf("want no error, got %q", errText)
	}

	// The same note cannot be pasted twice in a row.
	if err := chromedp.Run(ctx,
		chromedp.Click(".action-paste", chromedp.ByQuery),
		chromedp.WaitVisible(".error", chromedp.ByQuery),
		chromedp.Text(".error", &errText, chromedp.ByQuery),
		getNumberOfNotes(&numberOfNotes),
	); err != nil {
		t.Fatalf("paste failed: %v", err)
	}
	if numberOfNotes != 1 {
		t.Fatalf("want one note, got %d", numberOfNotes)
	}
	if errText != "This text is already pasted." {
		t.Fatalf("want an already-pasted error, got %q", errText)
	}

	// Marks.
	markText := ""
	textAfterReload := ""
	if err := chromedp.Run(ctx,
		selectTextRange(".note-content", 5, 9),
		chromedp.Sleep(100*time.Millisecond),
		chromedp.Click(".action-mark", chromedp.ByQuery),
		chromedp.WaitVisible("mark", chromedp.ByQuery),
		chromedp.Text("mark", &markText, chromedp.ByQuery),
		chromedp.Text(".note-content", &text, chromedp.ByQuery),
		chromedp.Text(".error", &errText, chromedp.ByQuery),
		chromedp.Reload(),
		chromedp.WaitVisible("mark", chromedp.ByQuery),
		chromedp.Text("mark", &textAfterReload, chromedp.ByQuery),
	); err != nil {
		t.Fatalf("mark failed: %v", err)
	}
	if text != textToPaste {
		t.Fatalf("want pasted note text to be unchanged after marking, got %q", text)
	}
	if markText != textToPaste[6:10] {
		t.Fatalf("want mark text to be %q, got %q", textToPaste[6:10], markText)
	}
	if errText != "" {
		t.Fatalf("want no error, got %q", errText)
	}
	if textAfterReload != textToPaste[6:10] {
		t.Fatalf("want mark text after reload to be %q, got %q", textToPaste[6:10], textAfterReload)
	}

	// Header and actions.
	var headerHidden bool
	if err := chromedp.Run(ctx,
		chromedp.Evaluate("!document.querySelector('.note-header').offsetParent", &headerHidden),
	); err != nil {
		t.Fatalf("something went wrong: %v", err)
	}
	if !headerHidden {
		t.Fatal("want note header to be hidden")
	}

	tagInputFocused := false
	if err := chromedp.Run(ctx,
		chromedp.Click(".action-show-note-header", chromedp.ByQuery),
		chromedp.WaitNotVisible(".action-show-note-header", chromedp.ByQuery),
		chromedp.WaitVisible(".note-header", chromedp.ByQuery),
		chromedp.WaitVisible(".input-add-tag", chromedp.ByQuery),
		chromedp.Evaluate("document.activeElement === document.querySelector('.input-add-tag')", &tagInputFocused),
	); err != nil {
		t.Fatalf("failed to toggle note header: %v", err)
	}
	if !tagInputFocused {
		t.Fatalf("want tag input to be focused")
	}

	// Add a tag.
	const newTag = "new tag"
	tagText := ""
	if err := chromedp.Run(ctx,
		chromedp.SendKeys(".input-add-tag", "something", chromedp.ByQuery),
		chromedp.Click(".note .icon-clear-input", chromedp.ByQuery),
		chromedp.SendKeys(".input-add-tag", newTag, chromedp.ByQuery),
		chromedp.SendKeys(".input-add-tag", kb.Enter, chromedp.ByQuery),
		chromedp.WaitVisible(".tag--applied", chromedp.ByQuery),
		chromedp.Text(".tag--applied", &tagText, chromedp.ByQuery),
		chromedp.Text(".error", &errText, chromedp.ByQuery),
	); err != nil {
		t.Fatalf("failed to add tag: %v", err)
	}
	if tagText != newTag {
		t.Fatalf("want tag to be %q, got %q", newTag, tagText)
	}
	if errText != "" {
		t.Fatalf("want no error, got %q", errText)
	}

	// Reload.
	var actionHidden, tagInputHidden bool
	if err := chromedp.Run(ctx,
		chromedp.Reload(),
		chromedp.WaitVisible(".note[data-id]", chromedp.ByQuery),
		chromedp.WaitNotPresent(".note-encrypted"),
		chromedp.Text(".note-content", &text, chromedp.ByQuery),
		getNumberOfNotes(&numberOfNotes),
		chromedp.Text(".tag.tag--applied", &tagText, chromedp.ByQuery),
		chromedp.Evaluate("!document.querySelector('.note-header').offsetParent", &headerHidden),
		chromedp.Evaluate("!document.querySelector('.action-show-note-header').offsetParent", &actionHidden),
		chromedp.Evaluate("!document.querySelector('.note-header .clearable-input').offsetParent", &tagInputHidden),
	); err != nil {
		t.Fatalf("reload failed: %v", err)
	}
	if numberOfNotes != 1 {
		t.Fatalf("want one note, got %d", numberOfNotes)
	}
	if text != textToPaste {
		t.Fatalf("want reloaded note text to be %q, got %q", textToPaste, text)
	}
	if tagText != newTag {
		t.Fatalf("want tag to be %q, got %q", newTag, tagText)
	}
	if headerHidden {
		t.Fatalf("want note header to be visible")
	}
	if !actionHidden {
		t.Fatalf("want note actions to be hidden")
	}
	if !tagInputHidden {
		t.Fatalf("want tag input to be hidden")
	}

	// Toggle tag input.
	if err := chromedp.Run(ctx,
		chromedp.Click(".action-new-tag", chromedp.ByQuery),
		chromedp.WaitVisible(".note-header .clearable-input input", chromedp.ByQuery),
	); err != nil {
		t.Fatalf("could not toggle tag input: %v", err)
	}

	if err := chromedp.Run(ctx,
		chromedp.Click(".action-new-tag", chromedp.ByQuery),
		chromedp.WaitNotVisible(".note-header .clearable-input input", chromedp.ByQuery),
	); err != nil {
		t.Fatalf("could not toggle tag input: %v", err)
	}

	// Main menu.
	if err := chromedp.Run(ctx,
		chromedp.Click(".icon-menu", chromedp.ByQuery),
		chromedp.WaitVisible(".main-menu-dropdown", chromedp.ByQuery),
		chromedp.Click(".icon-cancel", chromedp.ByQuery),
		chromedp.WaitNotVisible(".main-menu-dropdown", chromedp.ByQuery),
	); err != nil {
		t.Fatalf("could not show and then hide the main menu: %v", err)
	}

	// Default tag.
	const defaultTag = "default tag"
	const textToPaste2 = "second note"
	if err := chromedp.Run(ctx,
		chromedp.Click(".icon-menu", chromedp.ByQuery),
		chromedp.WaitVisible(".main-menu-dropdown", chromedp.ByQuery),
		chromedp.SendKeys(".input-default-tag", defaultTag, chromedp.ByQuery),
		chromedp.Click(".icon-cancel", chromedp.ByQuery),
		chromedp.WaitNotVisible(".main-menu-dropdown", chromedp.ByQuery),
		copyToClipboard(textToPaste2),
		chromedp.Click(".action-paste", chromedp.ByQuery),
		chromedp.WaitVisible(".note:nth-child(2)", chromedp.ByQuery),
		chromedp.WaitVisible(".note:nth-child(1) .tag.tag--applied", chromedp.ByQuery),
		chromedp.Text(".note:nth-child(1) .tag.tag--applied", &tagText, chromedp.ByQuery),
		chromedp.Text(".note:nth-child(1) .note-content", &text, chromedp.ByQuery),
		chromedp.Text(".error", &errText, chromedp.ByQuery),
		chromedp.Evaluate("!document.querySelector('.note:first-child .note-header').offsetParent", &headerHidden),
		chromedp.Evaluate("!document.querySelector('.note:first-child .action-show-note-header').offsetParent", &actionHidden),
		chromedp.Evaluate("!document.querySelector('.note:first-child .clearable-input').offsetParent", &tagInputHidden),
	); err != nil {
		t.Fatalf("could not add a note with a default tag: %v", err)
	}
	if errText != "" {
		t.Fatalf("want no error, got %q", errText)
	}
	if tagText != defaultTag {
		t.Fatalf("want tag to be %q, got %q", defaultTag, tagText)
	}
	if text != textToPaste2 {
		t.Fatalf("want second note text to be %q, got %q", textToPaste2, text)
	}
	if headerHidden {
		t.Fatalf("want note header to be visible")
	}
	if !actionHidden {
		t.Fatalf("want note actions to be hidden")
	}
	if !tagInputHidden {
		t.Fatalf("want tag input to be hidden")
	}

	// Remove tag.
	if err := chromedp.Run(ctx,
		chromedp.Click(".note:first-child .tag", chromedp.ByQuery),
		chromedp.WaitNotPresent(".note:first-child .tag", chromedp.ByQuery),
		chromedp.WaitNotVisible(".note:first-child .note-header", chromedp.ByQuery),
		chromedp.WaitVisible(".note:first-child .action-show-note-header", chromedp.ByQuery),
	); err != nil {
		t.Fatalf("could not remove a tag: %v", err)
	}

	// Backup.
	if err := chromedp.Run(ctx,
		page.SetDownloadBehavior(page.SetDownloadBehaviorBehaviorAllow).WithDownloadPath("testdata"),

		chromedp.Click(".icon-menu", chromedp.ByQuery),
		chromedp.WaitVisible(".main-menu-dropdown", chromedp.ByQuery),
		chromedp.Click(".action-backup", chromedp.ByQuery),
	); err != nil {
		t.Fatalf("could not remove a tag: %v", err)
	}

	var files []string
	for i := 0; i < 5; i++ {
		var err error
		files, err = filepath.Glob("testdata/pst-*-2.csv")
		if err != nil {
			t.Fatal("could not get the list of files in testdata/")
		}
		if len(files) > 0 {
			break
		}
		time.Sleep(time.Second)
	}

	if len(files) != 1 {
		t.Fatalf("want one .csv file, got %d", len(files))
	}
	data, err := ioutil.ReadFile(files[0])
	if err != nil {
		t.Fatalf("could not read %s: %v", files[0], err)
	}
	csvr := csv.NewReader(bytes.NewReader(data))
	records, err := csvr.ReadAll()
	if err != nil {
		t.Fatalf("could not read CSV: %v", err)
	}
	if len(records) != 2 {
		t.Fatalf("want 2 records in the downloaded CSV, got %d", len(records))
	}
	if records[0][2] != textToPaste {
		t.Fatalf("want first record text to be %q, got %q", textToPaste, records[0][2])
	}
	if records[1][2] != textToPaste2 {
		t.Fatalf("want second record text to be %q, got %q", textToPaste2, records[1][2])
	}
	if want := fmt.Sprintf(`["%s"]`, newTag); records[0][3] != want {
		t.Fatalf("want first record tags to be %q, got %q", want, records[0][3])
	}
	if records[1][3] != "" && records[1][3] != "[]" {
		t.Fatalf("want no second record tag, got %q", records[1][3])
	}
	if want := `["will"]`; records[0][4] != want {
		t.Fatalf("want first record marks to be %q, got %q", want, records[0][4])
	}

	// Pagination.
	for i := 0; i < 11; i++ {
		if err := chromedp.Run(ctx,
			copyToClipboard(fmt.Sprintf("note #%d", i+3)),
			chromedp.Click(".action-paste", chromedp.ByQuery),
		); err != nil {
			t.Fatalf("could not paste: %v", err)
		}
	}
	if err := chromedp.Run(ctx,
		chromedp.WaitVisible(".note:nth-child(13)", chromedp.ByQuery),
		chromedp.WaitNotPresent(".note:not([data-id])", chromedp.ByQuery),
		chromedp.Reload(),
		chromedp.WaitVisible(".note:nth-child(12)", chromedp.ByQuery),
		chromedp.WaitNotPresent(".note:nth-child(13)", chromedp.ByQuery),
		chromedp.Click(".action-next", chromedp.ByQuery),
		chromedp.WaitNotPresent(".note:nth-child(2)"),
		chromedp.Text(".note:nth-child(1) .note-content", &text),
	); err != nil {
		t.Fatalf("pagination failed: %v", err)
	}
	if text != textToPaste {
		t.Fatalf("want the first note on the second page, got %q", text)
	}
}

func clearTestData(t *testing.T) {
	tempfiles, _ := filepath.Glob("testdata/*")
	for _, fn := range tempfiles {
		if fn == "testdata/.gitkeep" {
			continue
		}
		if err := os.Remove(fn); err != nil {
			t.Log("could not remove " + fn)
		}
	}
}

func TestBasicFunctionality(t *testing.T) {
	os.Args = []string{"pst", "-db", "testdata/pst.db"}
	http.HandleFunc("/favicon.ico", func(w http.ResponseWriter, r *http.Request) {})
	clearTestData(t)
	defer clearTestData(t)

	go main()

	time.Sleep(1 * time.Second)

	c, _ := chromedp.NewExecAllocator(
		context.Background(),
		chromedp.Headless,
		chromedp.NoDefaultBrowserCheck,
		chromedp.NoFirstRun,
	)

	ctx, cancel := chromedp.NewContext(
		c,
		chromedp.WithLogf(t.Logf),
	)
	defer cancel()

	var exceptions []*runtime.ExceptionDetails
	var errors []*log.Entry
	chromedp.ListenTarget(ctx, func(ev interface{}) {
		switch ev := ev.(type) {
		case *log.EventEntryAdded:
			if ev.Entry.Level == log.LevelError {
				errors = append(errors, ev.Entry)
			}
		case *runtime.EventExceptionThrown:
			exceptions = append(exceptions, ev.ExceptionDetails)
		case *runtime.EventConsoleAPICalled:
			fmt.Printf("console.%s:\n", ev.Type)
			for _, arg := range ev.Args {
				fmt.Printf("  - %s: %s\n", arg.Type, arg.Value)
			}
		}
	})

	// No timeout for the first Run. See https://github.com/chromedp/chromedp/issues/513
	if err := chromedp.Run(ctx,
		browser.GrantPermissions([]browser.PermissionType{
			browser.PermissionTypeClipboardReadWrite,
			browser.PermissionTypeClipboardSanitizedWrite,
		}),
	); err != nil {
		t.Fatalf("could not start the browser and grant the necessary permissions: %v", err)
	}

	testInternal(t, ctx)

	toJSON := func(v interface{}) string {
		var b strings.Builder
		json.NewEncoder(&b).Encode(v)
		return b.String()
	}

	if len(exceptions) > 0 {
		t.Error("There were exceptions:")
		for _, e := range exceptions {
			t.Error(toJSON(e))
		}
	}

	if len(errors) > 0 {
		t.Error("There were errors:")
		for _, e := range errors {
			t.Error(toJSON(e))
		}
	}
}
