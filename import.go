// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"encoding/csv"
	"encoding/json"
	"io"
	"os"
	"time"

	"gitlab.com/opennota/pst/objectid"
	"go.etcd.io/bbolt"
)

func importFromCSV(db *bbolt.DB, filename string) error {
	f, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer f.Close()
	r := csv.NewReader(f)
	r.FieldsPerRecord = 5
	return db.Update(func(tx *bbolt.Tx) error {
		for {
			data, err := r.Read()
			if err == io.EOF {
				break
			}
			if err != nil {
				return err
			}
			created, err := time.Parse(time.RFC3339, data[0])
			if err != nil {
				return err
			}
			id, err := objectid.NewFromHex(data[1])
			if err != nil {
				return err
			}
			tags := make([]string, 0)
			if data[3] != "" {
				if err := json.Unmarshal([]byte(data[3]), &tags); err != nil {
					return err
				}
			}
			marks := make([]string, 0)
			if data[4] != "" {
				if err := json.Unmarshal([]byte(data[4]), &marks); err != nil {
					return err
				}
			}
			note := Note{
				ID:      data[1],
				Text:    data[2],
				Created: created,
				Tags:    tags,
				Marks:   marks,
			}
			if err := marshal(tx.Bucket([]byte("index")), id[:], note); err != nil {
				return err
			}
		}
		return nil
	})
}
