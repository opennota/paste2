pst [![License](http://img.shields.io/:license-agpl3-blue.svg)](http://www.gnu.org/licenses/agpl-3.0.html) [![Coverage report](https://gitlab.com/opennota/pst/badges/master/coverage.svg)](https://gitlab.com/opennota/pst/commits/master) [![Pipeline status](https://gitlab.com/opennota/pst/badges/master/pipeline.svg)](https://gitlab.com/opennota/pst/commits/master)
===

pst is a web application you can use to keep snippets of text, optionally
tagged.

You can mark selected text in your snippets, too. It'll be highlighted.

You'd also need a Chromium-based browser (e.g. Google Chrome), because Firefox
currently does not implement the [Clipboard
API](https://developer.mozilla.org/en-US/docs/Web/API/Clipboard_API).

pst is like [paste](https://gitlab.com/opennota/paste), but without any encryption and using BoltDB instead of MongoDB.

![Screenshot 1](./screenshot01.png)     ![Screenshot 2](./screenshot02.png)

## Install

    go install gitlab.com/opennota/pst@latest

## Donate

**Bitcoin (BTC):** `1PEaahXKwJvNJGJa2PXtPFLNYYigmdLXct`

**Ethereum (ETH):** `0x83e9607E693467Cb344244Df10f66c036eC3Dc53`
